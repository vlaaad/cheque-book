package chequebook;

import java.io.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.*;

/**
 * Created by rurik
 */
public class Bank implements Serializable {
    private static final long serialVersionUID = 1L;

    public static Bank instance = new Bank();

    private final Map<String, Person> persons = new HashMap<>();
    private String adminKey = "admin";

    public synchronized List<Person> getPersons() {
        return new ArrayList<>(persons.values());
    }

    public synchronized void addTransaction(Instant time, Person p1, Person p2, BigDecimal amount, String comment) {
        p1.getTransactions().add(new Transaction(time, p2, amount, comment));
        p2.getTransactions().add(new Transaction(time, p1, amount.negate(), comment));
        save();
    }

    public static void load() {
        try {
            byte[] data = (byte[]) Application.JDBC.queryForMap("SELECT data FROM bank WHERE id = 1").get("data");
            instance = (Bank) new ObjectInputStream(new ByteArrayInputStream(data)).readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void save() {
        try {
            System.out.println("saving...");
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            new ObjectOutputStream(out).writeObject(instance);
            Application.JDBC.update("INSERT INTO bank(id, data) VALUES (?, ?) ON CONFLICT(id) DO UPDATE SET data = ?;",
                    1, out.toByteArray(), out.toByteArray());
            System.out.println("ok");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized Person findPerson(String key) {
        Person p = persons.get(key);
        if (p == null) throw new IllegalArgumentException(key);
        return p;
    }

    public synchronized Person addPerson(String name) {
        Person person = new Person(UUID.randomUUID().toString(), name);
        persons.put(person.getKey(), person);
        save();
        return person;
    }

    public synchronized boolean isAdmin(String ctx) {
        return Objects.equals(ctx, adminKey);
    }

    public synchronized String generateAdminKey() {
        adminKey = UUID.randomUUID().toString();
        save();
        return adminKey;
    }
}
