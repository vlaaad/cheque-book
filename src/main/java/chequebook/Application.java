package chequebook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.util.Optional;

@SpringBootApplication
@ServletComponentScan(basePackageClasses = {ChequebookUI.Servlet.class})
public class Application implements ApplicationRunner {

    public static JdbcTemplate JDBC;

    @Bean
    public EmbeddedServletContainerCustomizer containerCustomizer() {
        return c -> c.setPort(Integer.valueOf(Optional.ofNullable(System.getenv("PORT")).orElse("8080")));
    }

    @Bean
    DataSource ds() {
        DriverManagerDataSource driver = new DriverManagerDataSource();
        driver.setDriverClassName("org.postgresql.Driver");
        driver.setUrl(Optional
                .ofNullable(System.getenv("JDBC_DATABASE_URL"))
                .orElse("jdbc:postgresql:///cb?user=cb&password=cb"));
        return driver;
    }

    @Autowired
    JdbcTemplate jdbc;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        jdbc.execute("CREATE TABLE IF NOT EXISTS bank(id INT PRIMARY KEY, data bytea NOT NULL);");
        JDBC = jdbc;
        Bank.load();
    }
}
