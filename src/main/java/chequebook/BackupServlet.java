package chequebook;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * Created by rurik
 * Usage:
 * - create backup:
 *   curl -J -O http://localhost:5000/backup/%%ADMIN_KEY%%
 * - restore backup:
 *   curl -F "file=@checkbook-backup-2017-04-06-01-37.bin" http://localhost:5000/backup/%%ADMIN_KEY|admin%%
 */
@WebServlet(BackupServlet.CONTEXT + "/*")
@MultipartConfig
public class BackupServlet extends HttpServlet {
    public static final String CONTEXT = "/backup";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        if (checkAccess(req)) {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            new ObjectOutputStream(out).writeObject(Bank.instance);
            res.setContentLength(out.size());
            res.setContentType("application/octet-stream");
            res.setHeader("Content-Disposition", "attachment; filename=\"checkbook-backup-" + now() + ".bin\"");
            ServletOutputStream os = res.getOutputStream();
            os.write(out.toByteArray());
            os.close();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        if (checkAccess(req)) {
            try {
                Bank.instance = (Bank) new ObjectInputStream(req.getParts().stream().map(e -> {
                    try {
                        return e.getInputStream();
                    } catch (IOException e1) {
                        throw new RuntimeException(e1);
                    }
                }).reduce(SequenceInputStream::new).orElseThrow(RuntimeException::new)).readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean checkAccess(HttpServletRequest req) {
        return Bank.instance.isAdmin(req.getRequestURI().substring(CONTEXT.length() + 1));
    }

    private static String now() {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm").withZone(ZoneId.systemDefault()).format(Instant.now());
    }
}
